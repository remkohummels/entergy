require('laravel-elixir-webpack');
var elixir = require('laravel-elixir');
var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-csso');

elixir(function (mix) {
    var bootstrapPath = 'node_modules/bootstrap-sass/assets';
    mix.sass('app.scss')
        .sass('pdf.scss')
        .scripts([
            '../../../node_modules/jquery/dist/jquery.min.js',
            '../../../node_modules/jquery-ui-bundle.1.12.1/jquery-ui.min.js',
            '../../../node_modules/jquery-validation/dist/jquery.validate.min.js',
            '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
            '../../../node_modules/datatables.net/js/jquery.dataTables.js',
            '../../../node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
            '../../../node_modules/yadcf/jquery.dataTables.yadcf.js',
            '../../../node_modules/datatables.net-buttons/js/dataTables.buttons.js',
            '../../../node_modules/datatables.net-buttons/js/buttons.html5.js',
            '../../../node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.js'
        ], 'public/js/app.js')
        .copy(bootstrapPath + '/fonts', 'public/fonts')
        .copy(bootstrapPath + '/javascripts/bootstrap.min.js', 'public/js');
});

// gulp.task('default', [ 'html', 'css' ]);
