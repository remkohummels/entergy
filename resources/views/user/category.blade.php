<li @if (isset($activeId) && ($category->id == $activeId)) class="active" @endif>
  <a href="{{ route('auction.category', $category->id) }}">
    {{ $category->name }} <span class="badge">{{ $category->auctions()->where('ended_at', null)->count() }}</span>
  </a>
</li>
