<div class="col-md-4">
  <div class="item clearfix">
    <span class="status bg-primary">New</span>
    <a href="{{ route('auction.detail', ['id' => $auction->id]) }}">
      <img class="img-responsive" src="{{ isset($auction->featuredImage()->path) ? $auction->featuredImage()->path : 'assets/default.png' }}">
    </a>
    <footer>
      <p>
        <a href="{{ route('auction.detail', ['id' => $auction->id]) }}" class="label label-primary">New in {{ $auction->category()->name }}</a>
      </p>
      <h4>
        <a href="{{ route('auction.detail', ['id' => $auction->id]) }}">
          {{ $auction->item_name }}<br>
          ${{ isset($auction->highestbid()->amount) ? $auction->highestbid()->amount : 0 }}
        </a>
      </h4>
      <p class="text-light">
        {{ $auction->bids->count() }} bids &nbsp; {{ $auction->timeRemain() }}
      </p>
    </footer>
  </div>
</div>
