<div class="col-md-4">
  <div class="item @if ($bid->auction()->highestbid()->amount > $bid->amount) item-danger @else clearfix @endif">
    @if ($bid->winner == 1)
    <span class="status bg-success">Won!</span>
    @elseif ($bid->auction()->ended_at)
    <span class="status bg-warning">Ended</span>
    @endif
    <a href="{{ route('auction.detail', ['id' => $bid->auction_id]) }}">
      <img class="img-responsive" src="{{ isset($bid->auction()->featuredImage()->path) ? $bid->auction()->featuredImage()->path : 'assets/default.png' }}">
    </a>
    <footer>
      @if ($bid->auction()->highestbid()->amount > $bid->amount)
        <p>
          <span class="label label-danger">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            You were outbid
          </span>
        </p>
      @endif
      <h4>
        <a href="{{ route('auction.detail', ['id' => $bid->auction_id]) }}">
          {{ $bid->auction()->item_name }}<br>${{ $bid->auction()->highestbid()->amount }}
        </a>
      </h4>
      <p class="text-light">
        {{ $bid->auction()->bids->count() }} bids &nbsp;
        <span class="@if ($bid->winner == 1) text-success @elseif ($bid->auction()->ended_at) text-warning @endif">
          {{ $bid->auction()->timeRemain() }}
        </span>
      </p>
    </footer>
  </div>
</div>
