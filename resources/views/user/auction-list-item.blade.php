<div class="col-xs-6 col-sm-4">
  <div class="item clearfix">
    <a href="{{ route('auction.detail', ['id' => $auction->id]) }}"><img class="img-responsive" src="{{ isset($auction->featuredImage()->path) ? $auction->featuredImage()->path : '/assets/default.png' }}"></a>
    <footer>
      <h4><a href="{{ route('auction.detail', ['id' => $auction->id]) }}">{{ $auction->item_name }}<br>
        ${{ $auction->bid_amount }}
      </a></h4>
      <p class="text-light">{{ $auction->bids->count() }} bids &nbsp; Time left:&nbsp;<span class="closing text-danger">{{ $auction->timeRemain() }}</span></p>
    </footer>
  </div>
</div>
