@extends('layouts.app')

@section('content')
  @if( Session::has('token_mismatch'))
  <div class="alert alert-warning alert-dismissible" role="alert" id="alert" style="text-align: center;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <strong>
      {{ Session::get('token_mismatch') }}<br>
      Please login again.
    </strong>
  </div>
  @endif

  <div class="panel panel-default">
    <div class="panel-heading"><strong>Log in to Entergy Bid Board</strong></div>
    <div class="panel-body">
      <form role="form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <input id="email" type="email" class="form-control input-lg" name="email" value="{{ $email or old('email') }}" required
                 @if (!isset($email)) autofocus @endif
                 placeholder="Entergy email address...">

          @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <input id="password" type="password" class="form-control input-lg" name="password" required
                 @if (isset($email)) autofocus @endif
                 placeholder="Your password...">

          @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
          @endif
        </div>

        <div class="form-group">
          <p>
            <button type="submit" class="btn btn-primary btn-block btn-lg">
              Log In
            </button>
          </p>
          <a class="btn btn-link" href="{{ route('password.request') }}">
            Forgot Your Password?
          </a>
        </div>
      </form>
    </div>
  </div>
  <p class="text-center">Don't have an account? <a href="{{ route('register') }}">Sign Up</a></p>
@endsection
