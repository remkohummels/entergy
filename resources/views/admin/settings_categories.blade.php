<div class="col-sm-6">
  <div class="jumbotron">
    <h2>Categories</h2>
    <p>
      <a href="#" class="btn btn-success" data-toggle="modal" data-target="#createCategory">Add Category</a>
    </p>
    <table class="table table-striped bids">
      <tbody>
      @each('admin.settings_categories_item', $categories, 'category')
      </tbody>
    </table>
  </div>
</div>
