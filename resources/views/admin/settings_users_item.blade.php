<tr>
  <td colspan="4">
    <a href="#">{{$user->name}}</a>
  </td>
  <td>
    Added {{$user->updated_at->toFormattedDateString()}}
  </td>
  <td>
    @if ($user->id == Auth::user()->id)
      <span class="pull-right">N/A</span>
    @else
      <a href="{{ route('admin.revoke', $user->id) }}" class="text-danger pull-right">Delete</a>
    @endif
  </td>
</tr>
