<div class="col-sm-6">
  <div class="jumbotron">
    <h2>Business Units</h2>
    <p>
      <a href="#" class="btn btn-success" data-toggle="modal" data-target="#createBusinessUnit">Add Unit</a>
    </p>
    <table class="table table-striped bids">
      <tbody>
      @each('admin.settings_businessunits_item', $businessunits, 'businessunit')
      </tbody>
    </table>
  </div>
</div>
