<div class="col-sm-6">
  <div class="jumbotron">
    <h2>Admin Users</h2>
    <p>
      <a href="#" class="btn btn-success" data-toggle="modal" data-target="#createAdmin">Add Admin</a>
    </p>
    <table class="table table-striped bids">
      <tbody>
      @each('admin.settings_users_item', $users->where('admin', true), 'user')
      </tbody>
    </table>
  </div>
</div>
