<tr>
  <td>
    ${{$bid->amount}} by <strong>{{$bid->author()->name}}</strong> ({{$bid->author()->email}})
    @if ($bid->auction()->ended_at != null && $bid->winner == true)
      <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
    @endif
  </td>

  <td>
    @if ($bid->auction()->ended_at == null)
      <a href="#" class="pull-right" data-toggle="modal" data-target="#AuctionCloseModal">Make winner</a>
    @else
      <a href="{{URL::route('bids.winner', ['id' => $bid->id])}}" class="pull-right">Make winner</a>
    @endif
  </td>
  <td>
    <a href="{{URL::route('bids.destroy', ['id' => $bid->id])}}" class="text-danger deleteBidhref">Delete</a>
  </td>
  <td>
    {{$bid->created_at->timezone('America/Chicago')->toFormattedDateString()}} {{$bid->created_at->timezone('America/Chicago')->format('g:i a T')}}
  </td>
</tr>
