<div class="modal fade modal-category" id="createCategory" tabindex="-1" role="dialog" aria-labelledby="Create Category">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Create Category</h4>
      </div>
      <div class="modal-body dark-bg">
        <div class="row">
          {!! BootForm::openHorizontal(['sm' => [2, 10]])->action(URL::route('category.store'))->id('addCategoryForm'); !!}
          <div class="col-sm-10">
          {!! BootForm::text('Name', 'category_name')->id('categoryNameField'); !!}
          </div>
          <div class="col-sm-2">
          {!! BootForm::submit('Add', 'btn btn-success') !!}
          </div>
          {!! BootForm::close() !!}
        </div>
        <div id="addCategoryAlert" class="row">
          <div class="col-sm-12">
            <div class="alert alert-warning alert-dismissible" role="alert" id="alertAddCategoryLabel" style="margin: 20px 0 0;">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Category name cannot be empty.</strong>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
