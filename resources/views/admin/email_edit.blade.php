<div class="clearfix spacer"></div>
<h1>Review and edit email</h1>

<hr>
<div class="row">
  <div class="col-sm-10 col-md-8 col-lg-7">
    {!! BootForm::openHorizontal(['sm' => [4, 8]])->action(URL::route('admin.sendEmail')); !!}
    {!! BootForm::text('To:', 'mail_to_shown')->value($auction->winnerbid()->author()->name.' - '.$auction->winnerbid()->author()->email)->disable() !!}
    {!! BootForm::hidden('mail_to')->value($auction->winnerbid()->author()->email) !!}
    {!! BootForm::hidden('id')->value($auction->id) !!}
    {!! BootForm::text('Subject:', 'mail_subject')->value($mail_subject) !!}
    <hr>
    {!! BootForm::textarea('Message:', 'mail_body')->value($mail_body) !!}
    {!! BootForm::textarea('Signature:', 'mail_signature')->value($mail_signature) !!}
    <hr>
    <div class="spacer"></div>
    {!! BootForm::submit('Send email', 'btn btn-lg btn-block btn-success') !!}
    {!! BootForm::close() !!}
    <div class="spacer"></div>
  </div>
</div>
