<div class="modal fade modal-category" id="createAdmin" tabindex="-1" role="dialog" aria-labelledby="Add Admin">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Admin</h4>
      </div>
      <div class="modal-body dark-bg">
        <div class="row">
          {!! BootForm::openHorizontal(['sm' => [2, 10]])->action(URL::route('admin.add'))->id('addAdminForm') !!}
          <div class="col-sm-10 autocomplete-list">
          {!! BootForm::text('Name', 'name')->id('qu') !!}
          {!! BootForm::hidden('id', 'id')->id('h') !!}
          </div>
          <div class="col-sm-2">
          {!! BootForm::submit('Add', 'btn btn-success') !!}
          </div>
          {!! BootForm::close() !!}
        </div>
        <div id="addAdminAlert" class="row">
          <div class="col-sm-12">
            <div class="alert alert-warning alert-dismissible" role="alert" id="alertAddAdminLabel" style="margin: 20px 0 0;">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>User does not exist or is already an Administrator.</strong>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
