<tr>
  <td colspan="3">
    <a href="#">{{$category->name}}</a>
  </td>
  <td>
    <form id="delete-form" action="{{ route('category.destroy', $category->id) }}" method="POST">
      {{ method_field('DELETE') }}
      {{ csrf_field() }}
      <button type="submit" class="btn-link text-danger pull-right btn-danger">Delete</button>
    </form>
  </td>
</tr>
