<nav class="navbar navbar-admin">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-4" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{action('AdminDashboardController@index')}}">
        <img alt="Brand" src="/assets/logo.png" style="height: 40px;">
      </a>
      <h4 class="navbar-text">Bid Board Admin</h4>
    </div>
    @if (Auth::user() !=  null && Auth::user()->admin == true)
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="{{ action('AdminSettingsController@index') }}">Settings</a></li>
          <li><a href="{{ action('AuctionController@index') }}">Browse</a></li>
          <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
              Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </li>
          <li>
            <button href="newitem.html" type="submit" class="btn btn-success navbar-btn" onclick="location.href='{{URL::route('auctions.create')}}'">New
              Auction
            </button>
          </li>
        </ul>
        {!! BootForm::open()->addClass('navbar-form navbar-right')->action(URL::route('search.auctions_admins'))->get(); !!}
        {!! BootForm::text('', 'q')->placeholder('Search bid board') !!}
        {!! BootForm::submit('Go', 'btn btn-default') !!}
        {!! BootForm::close() !!}
      </div>
    @endif
  </div>
</nav>
