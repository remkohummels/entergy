<div class="modal fade modal-businessunit" id="createBusinessUnit" tabindex="-1" role="dialog" aria-labelledby="Create Unit">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Create Business Unit</h4>
      </div>
      <div class="modal-body dark-bg">
        <div class="row">
          {!! BootForm::openHorizontal(['sm' => [2, 10]])->action(URL::route('businessunit.store'))->id('addBUForm'); !!}
          <div class="col-sm-10">
          {!! BootForm::text('Name', 'businessunit_name')->id('buNameField') !!}
          </div>
          <div class="col-sm-2">
          {!! BootForm::submit('Add', 'btn btn-success') !!}
          </div>
          {!! BootForm::close() !!}
        </div>
        <div id="addBUAlert" class="row">
          <div class="col-sm-12">
            <div class="alert alert-warning alert-dismissible" role="alert" id="alertAddBULabel" style="margin: 20px 0 0;">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Business Unit name cannot be empty.</strong>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
