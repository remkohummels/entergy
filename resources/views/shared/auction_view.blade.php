@if (Auth::user()->admin == 1 && $admin_flag)
  @include('admin.auction-tools')
@endif

<h1>{{$auction->item_name}} <small class="text-muted">@if (Auth::user()->admin == 1 && $admin_flag == null) (<a href="{{ action('AuctionController@show', ['id' => $auction->id]) }}">admin</a>) @endif</small></h1>

<div class="row">

  <div class="col-sm-5 col-md-6">
    <img class="img-responsive img-featured" src="/{{isset($auction->featuredImage()->path) ? $auction->featuredImage()->path : 'assets/default.png'}}">
    <div class="row">
      @if ($auction->images()->count()>1)
      @each('shared.auction_additional_images', $auction->images(), 'image')
      @endif
    </div>
  </div>

  <div class="col-sm-7 col-md-6">
    <h1 class="current-bid">
      ${{ $auction->currentBidAmount() }} <span class="small gray-light">current bid</span>
    </h1>
    <table class="table table-striped auction-details">
      <tr>
        <td colspan="2">
          <label>Description</label>
          <p>{{$auction->item_description}}</p>
        </td>
      </tr>
      <tr>
        <td>
          <label>Condition</label>
        </td>
        <td>
          <p>{{$auction->item_condition}}</p>
        </td>
      </tr>
      <tr>
        <td>
          <label>Time left</label>
        </td>
        <td>
          <p class="text-danger">{{$auction->timeRemain()}}</p>
        </td>
      </tr>
      @if ($auction->ended_at == null)
      <tr>
        <td>
          <label>Ends at</label>
        </td>
        <td>
          <p>{{Carbon\Carbon::parse($auction->expires_at)->timezone('America/Chicago')->format('n/j/y \a\t g:i:s a T')}}</p>
        </td>
      </tr>
      @endif
      @if ($auction->ended_at != null && $auction->winnerBid() != null && (Auth::user() == $auction->winnerBid()->author() || Auth::user()->admin == 1 && $admin_flag) )
      <tr>
        <td>
          <label>Bill Of Sale:</label>
        </td>
        <td>
          <a href="{{route('auction.bos', ['id' => $auction->id]) }}">Download</a>
        </td>
      </tr>
      @endif
    </table>
    @if (is_null($auction->ended_at) && !$admin_flag)
    <form class="bid clearfix img-rounded dark-bg" id="bidForm">
      <div class="fieldset disabled">
        <div class="row">
          <div class="col-sm-4">
            <label>Bid price</label>
            <div class="input-group">
              <div class="input-group-addon">$</div>
              <input type="number" id="init_price" class="form-control input-lg" min="{{ (isset($auction->highestbid()->amount) && $auction->highestbid()->amount >= $auction->min_bid) ? $auction->highestbid()->amount + 1 : $auction->min_bid }}"
                     placeholder="{{ (isset($auction->highestbid()->amount) && $auction->highestbid()->amount >= $auction->min_bid) ? $auction->highestbid()->amount + 1 : $auction->min_bid }}" required autofocus>
            </div>
            <div class="clearfix"></div>
            <label>Minimum bid: ${{ (isset($auction->highestbid()->amount) && $auction->highestbid()->amount >= $auction->min_bid) ? $auction->highestbid()->amount + 1 : $auction->min_bid }}</label>
          </div>
          <div class="col-sm-5 col-sm-offset-3">
            <label>&nbsp;</label><br>
            <input type="submit" value="Start Bid" class="btn btn-lg btn-block btn-success">
          </div>
        </div>
      </div>
    </form>

    <div class="spacer clearfix"></div>

    @if (Auth::user()->current_bid($auction->id))
    <div class="row">
      <div class="col-xs-6">
        <div class="dropdown dropdown-form">
          <button type="button" class="btn @if (is_null($watch)) btn-primary @else btn-success @endif"
                  id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span class="glyphicon glyphicon-ok @if (is_null($watch)) hidden @endif" aria-hidden="true"></span> Get Alerts
          </button>
          <form class="dropdown-menu" aria-labelledby="dropdownMenu1" method="POST" action="{{ Request::url() . '/alert' }}">
            {{ csrf_field() }}
            <div class="form-group">
              <label>
                <input type="checkbox" name="notify_email" @if (!is_null($watch) && $watch->notify_email) checked @endif>
                &nbsp;Get&nbsp;email&nbsp;notifications
              </label>
            </div>
            <div class="form-group">
              <label>
                <input type="checkbox" name="notify_phone" @if (!is_null($watch) && $watch->notify_phone) checked @endif
                @if (is_null(Auth::user()->phone)) disabled @endif>
                &nbsp;Get&nbsp;text&nbsp;notifications
              </label>
            </div>
            <div class="form-group">
              <input type="submit" class="btn btn-success" value="Confirm">
              <input type="button" class="btn btn-default" value="Cancel" onclick="$('#dropdownMenu1').dropdown('toggle');">
            </div>
          </form>
        </div>
      </div>
    </div>
    @endif

    @endif
  </div>
</div>

<hr>

<div class="row">
  <div class="col-sm-4">
    <div class="form-group clearfix">
      <label>Full Description</label>
      <p>{{$auction->item_description}}</p>
    </div>
    <div class="form-group clearfix">
      <label>IR Number</label>
      <p>{{ $auction->getIR() }}</p>
    </div>
    <div class="form-group clearfix">
      <label>Disclaimer</label>
      <p>Unit is in {{$auction->item_condition}} CONDITION. Buyer must be able to pick up item from site. All items sold on the employee bid board are sold "AS
        IS", "WHERE IS" and there are no implied or expressed warranties. Buyer bears all responsibilities. Investment Recovery Services reserves the right to discontinue
        or reject any bids at any time during this process.</p>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group clearfix">
      <label for="" class="control-label">Location</label>
      <p>{{ isset($auction->location) ? $auction->location : 'N/A' }}</p>
    </div>
    <div class="form-group clearfix">
      <label for="" class="ccontrol-label">Contact name</label>
      <p>{{$auction->contact_name}}</p>
    </div>
    <div class="form-group clearfix">
      <label for="" class="control-label">Contact phone</label>
      <p>{{$auction->contact_phone}}</p>
    </div>
    <div class="form-group clearfix">
      <label for="" class="control-label">Business unit</label>
      <p>{{$auction->businessunit()->name}}</p>
    </div>
  </div>
  @if (!$admin_flag)
  <div class="col-sm-5 bids">
    <label>Bids</label>
    <div class="row">
      @if (Auth::user()->current_bid($auction->id))
      <div class="col-xs-7"><strong class="text-success">Your bid ${{ Auth::user()->current_bid($auction->id)->amount }}</strong></div>
      <div class="col-xs-5"><strong class="text-success">{{ Auth::user()->current_bid($auction->id)->created_at->timezone('America/Chicago')->format('n/j/y \a\t g:i:s a T') }}</strong></div>
      @else
      <div class="col-xs-12"><strong class="text-danger">You did not start a bid on this auction yet!</strong></div>
      @endif
    </div>
    <!-- Someone's bid history else -->
    @each('shared.auction_bid_history', $others_bids, 'other_bid')
    <!-- End of Someone's bid history else -->
  </div>
  @endif
</div>
