<nav class="navbar navbar-default navbar-inverse" style="margin-bottom: 0;">
  <div class="container-fluid">
    <div class="navbar-header">
      @if (Auth::user())
      <button type="button" class="collapsed navbar-toggle" data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-4" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      @endif
      <a class="navbar-brand" href="{{ route('auctions.index') }}">
        <img alt="Brand" src="{{ asset('assets/logo.png') }}" style="height: 40px;">
      </a>
    </div>

    @if (Auth::user())
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">
      <h4 class="navbar-text">Employee Bid Board</h4>
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::check() && Auth::user()->admin == true)
        <li><a href="{{ route('admin.dashboard') }}">Admin</a></li>
        @endif
        <li><a href="{{ route('alerts.index') }}">Alerts</a></li>
        <li><a href="{{ route('bids.index') }}">My Bids</a></li>
        <li><a href="{{ route('user.profile') }}">My Info</a></li>
        <li>
          <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            Logout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
      </ul>
      {!! BootForm::open()->addClass('navbar-form navbar-right')->action(URL::route('search.auctions'))->get(); !!}
      {!! BootForm::text('', 'q')->placeholder('Search bid board') !!}
      {!! BootForm::submit('Go', 'btn btn-default') !!}
      {!! BootForm::close() !!}
    </div>
    @endif

  </div>
</nav>
