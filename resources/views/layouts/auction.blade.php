<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <title>Entergy</title>
</head>
<body>
  {{-- This is the Navbar, ideally it must change based on the User role--}}
  @if (Auth::user()->admin == 1 && $admin_flag)
    @include('admin.navbar')
  @else
    @include('shared.navbar')
  @endif
  {{-- Breadcrumbs from shared --}}
  @if (Auth::user()->admin == 0 || !$admin_flag)
    @include('shared.breadcrumbs')
  @endif
  <!-- ALERTS -->
  @if (isset($bid_flag) && $bid_flag))
  <div class="container">
    <div class="alert alert-success alert-dismissible" role="alert" id="alert" style="margin: 20px 0 0;">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Your bid was entered.</strong><br>
      You can enable/disable your email & text notification with Alerts button if another bid is entered or if this auction is coming to an end within an hour.
    </div>
  </div>
  @endif
  @if (isset($expired_flag) && $expired_flag == true))
  <div class="container">
    <div class="alert alert-warning alert-dismissible" role="alert" id="alert" style="margin: 20px 0 0;">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Your bid was NOT entered.</strong><br>
      The auction ended before you bid was submitted.
    </div>
  </div>
  @endif
  <!-- END ALERT -->
  <div class="container">
    @include('shared.auction_view')
    <hr>
    @if (Auth::user()->admin == 1 && $admin_flag)
      @include('admin.activity_log')
    @endif
    @include('shared.footer')
  </div>
  <!-- BID Modal -->
  @include('user.auction-bid-modal')
  <!-- End of Bid Modal -->

  <!-- Admin email modal for auction -->
  @if ($auction->ended_at != null && Auth::user()->admin == 1)
    @include('admin.auction_email_modal')
  @endif
  <!-- End of Admin email modal for auction -->

  <!-- Admin modal for closing auction -->
  @if ($auction->ended_at == null && Auth::user()->admin == 1)
    @include('admin.auction_close_modal')
  @endif
  <!-- End of Admin modal for closing auction -->

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>
  <script>
      // check the price input validation, then open the bid modal
      $("#bidForm").on('submit', function(e){
          e.preventDefault();
          $('#bid_price').val($('#init_price').val());
          $('#bidModal').modal('show');
          return false;
      });

      // Enable/Disable the submit button on 'Terms' checkbox checked event
      $('#terms_checkbox').click(function() {
          if ($(this).is(':checked')) {
              $('#submit_bidForm').removeAttr('disabled');
          } else {
              $('#submit_bidForm').attr('disabled', 'disabled');
          }
      });

      // submit the Bid Form
      $('#submit_bidForm').click(function() {
          $( "#bidModalForm" ).submit();
      });

      // change the featured image on rollover
      $('.img-thumbs').on('mouseover', function(){
         var img = $(this).attr('src');
         $('.img-featured').attr('src', img);
      });
  </script>
</body>
</html>
