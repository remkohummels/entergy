<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Entergy</title>
  </head>
  <body>
    {{-- This is the Navbar, ideally it must change based on the User role--}}
    @if (Auth::user()->admin == 1 && $admin_flag)
      @include('admin.navbar')
    @else
      @include('shared.navbar')
    @endif
    {{-- End of the Navbar. Main container starts --}}

    <div class="container">
      @if ($search_count > 0)
      <h1>{{$search_count}} Results for "{{$search_term}}"</h1>
      @endif
      <div class="row items">
        @if (Auth::user()->admin == 1 && $admin_flag)
          @each('admin.auction-search-list-item', $auctions, 'auction', 'user.auction-empty-list')
        @else
          @each('user.auction-list-item', $auctions, 'auction', 'user.auction-empty-list')
        @endif
      </div>

      {{-- This is the footer section --}}
      @include('shared.footer')
      {{-- End of footer --}}
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>
