<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <title>Entergy</title>
</head>
<body>
  {{-- This is the Navbar, ideally it must change based on the User role--}}
  @include('shared.navbar')
  {{-- End of the Navbar. Main container starts --}}
  <div class="container">

    <div class="row">
      <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">

        @include('shared.login')

      </div>
    </div>

    <footer>
      <hr>
      <p>&copy; 2017 Entergy, Inc.</p>
    </footer>
  </div>
</body>
</html>
