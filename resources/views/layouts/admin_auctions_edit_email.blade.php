<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <title>Entergy</title>
</head>
<body>
  {{-- This is the Navbar, ideally it must change based on the User role--}}
  @if (Auth::user()->admin == 0)
    @include('shared.navbar')
  @else
    @include('admin.navbar')
  @endif

  {{-- Breadcrumbs from shared --}}
  @include('shared.breadcrumbs')
  <div class="container">
    @include('admin.email_edit')

    @include('shared.footer')
  </div>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>

  @if ($auction->ended_at != null && Auth::user()->admin == 1)
    @include('admin.auction_email_modal')
  @endif
</body>
</html>
