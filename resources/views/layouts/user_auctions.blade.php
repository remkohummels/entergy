<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Entergy</title>
  </head>
  <body>
    {{-- This is the Navbar, ideally it must change based on the User role--}}
    @include('shared.navbar')
    @include('user.categories')
    {{-- End of the Navbar. Main container starts --}}

    <div class="container">
      @include('shared.sort-menu')

      <div class="row items">
        @each('user.auction-list-item', $auctions, 'auction', 'user.auction-empty-list')
      </div>

      {{-- This is the footer section --}}
      @include('shared.footer')
      {{-- End of footer --}}
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>
