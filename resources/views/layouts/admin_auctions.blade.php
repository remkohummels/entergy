<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <title>Entergy</title>
</head>
<body>
  {{-- This is the Navbar, ideally it must change based on the User role--}}
  @include('admin.navbar')
  {{-- End of the Navbar. Main container starts --}}
  <!-- ALERTS -->
  @if (isset($auction) && $auction->ended_at != null)
    <div class="container">
      <div class="alert alert-danger alert-dismissible" role="alert" id="alert" style="margin: 20px 0 0;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong>This auction is closed</strong><br>
        Please select a new end date if you would like to reopen it<br>
        This auction ended at {{$auction->ended_at}} and
        @if ($auction->winnerBid() !=  null)
          was awarded to {{$auction->winnerBid()->author()->name}} ({{$auction->winnerBid()->author()->email}}).
        @else
          had no winner.
        @endif
        <br>
      </div>
    </div>
  @endif
  <!-- END ALERT -->
  <div class="container">
    @if (isset($auction))
      @include('admin.auctions_edit')
    @else
      @include('admin.auctions_create')
    @endif

    @include('shared.footer')
  </div>
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>
  <script>
      // Date Picker for /admin/auctions/create
      $(function () {
          $("#datepicker").datepicker();
          $("#datepicker").datepicker("option", "dateFormat", "m/d/yy");
          $("#ui-datepicker-div").hide();
      });


      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {

          var input = document.getElementById('searchLocation');
          var autocomplete = new google.maps.places.Autocomplete(input);

      }
  </script>
  @if (isset($auction))
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.image-delete').on('click', function () {
            var img_id = $(this).data('img_id');
            if ($('input[value=' + img_id + ']:checked').length > 0 == true) {
                $('#auctionDeleteModal').modal('show');
            } else {
                $.ajax({
                    type: 'post',
                    url: "{{ action('ImageController@delete') }}",
                    data: {id: img_id},
                    success: function () {
                        location.reload();
                    }
                });
            }
            ;

        });

        $('.image-feature').on('click', function () {
            var img_id = $(this).val();
            $.ajax({
                type: 'post',
                url: "{{ action('ImageController@setFeatured') }}",
                data: {id: img_id}
            });

        });
    </script>
  @endif
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYyDhqLevtYuCTv3y7FYCl76KFD742m4U&libraries=places&callback=initMap" async defer></script>
</body>
</html>
