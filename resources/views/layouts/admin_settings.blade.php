<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <title>Entergy</title>
</head>
<body>
  {{-- This is the Navbar, ideally it must change based on the User role--}}
  @include('admin.navbar')
  {{-- End of the Navbar. Main container starts --}}
  <div class="container">

    <div class="spacer clearfix"></div>

    <div class="row">
      <div class="col-sm-4">
        <h1 style="margin: 0;">Auction Settings</h1>
        <hr>
        <div class="spacer clearfix show-xs"></div>
      </div>
    </div>
    <div class="row">
      @include('admin.settings_businessunits')
      @include('admin.settings_categories')
      @include('admin.settings_users')
    </div>


    @include('shared.footer')
  </div>

  <!-- Modals -->
  @include('admin.modal_settings_category')
  @include('admin.modal_settings_businessunit')
  @include('admin.modal_settings_admin')

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>
  <script>

      // Autocomplete for /admin/settings
      $(function () {
          $("#qu").autocomplete({
              source: "user",
              delay: 300,
              minLength: 2,
              select: function (event, ui) {
                  $('#addAdminAlert').hide();
                  $('#qu').val(ui.item.value);
                  $('#h').val(ui.item.id);
                  $('#qu').prop('disabled', true);
              }
          });
          $("#qu").autocomplete("option", "appendTo", ".autocomplete-list");
      });
      $('#addAdminForm').submit(function () {
          if ($('#h').val().length < 1) {
              $('#addAdminAlert').show();
              return false;
          }
      });
      $('#createAdmin').on('hidden.bs.modal', function () {
          $('#qu').val('');
          $('#h').val('');
          $('#qu').prop('disabled', false);
      })
      $('#addBUForm').submit(function () {
          if ($('#buNameField').val().length < 1) {
              $('#addBUAlert').show();
              return false;
          }
      });
      $('#addCategoryForm').submit(function () {
          if ($('#categoryNameField').val().length < 1) {
              $('#addCategoryAlert').show();
              return false;
          }
      });
      $('#addAdminAlert').hide();
      $('#addBUAlert').hide();
      $('#addCategoryAlert').hide();
  </script>
</body>
</html>
