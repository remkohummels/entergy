<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Common Routes
Auth::routes();
Route::get('/', 'HomeController@index');


// Common Routes.
// Auction's routes that extends the CRUD resource need to be defined before the Resource routes
Route::get('auctions/{id}/bos', ['uses' => 'AuctionController@bos', 'as' => 'auction.bos', 'middleware' => ['auth']]);

// Frontend Routes
{
    Route::get('register/verify', 'Auth\RegisterController@showVerify');
    Route::post('register/verify', ['uses' => 'Auth\RegisterController@verify', 'as' => 'register.verify']);
    Route::resource('auctions', 'AuctionController',  ['except' => ['create', 'store', 'update', 'destroy', 'edit']]);
    Route::post('auctions', 'AuctionController@sort');
    Route::get('auctions/{id}', ['uses' => 'AuctionController@show', 'as' => 'auction.detail']);
    Route::post('auctions/{id}', 'AuctionController@createBid');
    Route::post('auctions/{id}/alert', 'AlertController@auctionAlert');
    Route::get('auctions/category/{id}', ['uses' => 'AuctionController@getCategory', 'as' => 'auction.category']);
    Route::post('auctions/category/{id}', 'AuctionController@sort');
    Route::post('auctions/category/{id}/alert', 'AlertController@categoryAlert');
    Route::resource('bids', 'BidController', ['except' => ['create', 'store', 'update', 'destroy']]);
    Route::resource('alerts', 'AlertController', ['except' => ['create', 'store', 'update', 'destroy']]);
    Route::get('profile', ['uses' => 'UserController@index', 'as' => 'user.profile']);
    Route::post('profile', 'UserController@update');
    Route::get('search', ['uses' => 'SearchController@auctions', 'as' => 'search.auctions']);
}

// Admin Routes
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', function () {
        return redirect()->route('admin.dashboard');
    });
    Route::get('dashboard', ['uses' => 'AdminDashboardController@index', 'as' => 'admin.dashboard']);
    Route::get('settings', 'AdminSettingsController@index');
    Route::post('auctions/close', ['uses' => 'AuctionController@close', 'as' => 'auctions.close']);
    Route::resource('auctions', 'AuctionController', ['only' => ['show', 'create', 'store', 'update', 'destroy', 'edit']]);
    Route::resource('category', 'CategoryController', ['only' => ['store', 'destroy']]);
    Route::resource('businessunit', 'BusinessunitController', ['only' => ['store', 'destroy']]);
    Route::get('user', ['uses' => 'UserController@autocomplete']);
    Route::post('add/user', ['uses' => 'UserController@addAdmin', 'as' => 'admin.add']);
    Route::get('revoke/user/{id}', ['uses' => 'UserController@revokeAdmin', 'as' => 'admin.revoke']);
    Route::any('api/images/setFeatured', ['uses' => 'ImageController@setFeatured', 'as' => 'image.setFeatured']);
    Route::any('api/images/delete', ['uses' => 'ImageController@delete', 'as' => 'image.delete']);
    Route::get('auctions/{id}/email/{type}', ['uses' => 'AdminAuctionsEmailController@editEmail', 'as' => 'admin.editEmail']);
    Route::post('auctions/email/send', ['uses' => 'AdminAuctionsEmailController@sendEmail', 'as' => 'admin.sendEmail']);
    Route::get('bids/destroy/{id}', ['uses' => 'BidController@destroy', 'as' => 'bids.destroy']);
    Route::get('bids/winner/{id}', ['uses' => 'BidController@winner', 'as' => 'bids.winner']);
    Route::get('search', ['uses' => 'SearchController@auctions', 'as' => 'search.auctions_admins']);
});

// -- Special Route: read and return the image
Route::get('public/auction_images/{filename}', 'ImageController@show');
Route::get('auctions/category/public/auction_images/{filename}', 'ImageController@show');
