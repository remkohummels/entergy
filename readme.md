# Auction/Bid Web App
###### The Laravel application running on Digital Ocean Ubuntu/Apache Server
***

## General Requirements

The system was developed using [Laravel Framework 5.4.13](https://laravel.com/docs/5.4/)

Laravel 5.4.13 has the following server requirements:

1. PHP >= 5.6.4 (the system has been tested with PHP 7.1.0)

The following extensions for PHP are required
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

2. Laravel provides abstraction from Databases providers and all Laravel supported Databases should support the system, however the system was developed and tested using MySQL 5.7.16

3. Laravel works over different web servers, as long as they support the needed PHP version, but the system was developed over nginx 1.11.5 and was tested running on Apache on staging server.

## Development requirements

1. NPM => 3.10.8
2. Gulp => 3.9.1
3. Composer => 1.4

This tools are required on development environments and optional for production environments, as long as you compile the assets locally and deploy the static files into the server. (See the installation guide: [System's Deployment](https://bitbucket.org/remko93223/entergy/wiki/System's%20deployment))

## Homestead Environment

The system was partially developed using Laravel's [Homestead Development Environment](https://laravel.com/docs/5.4/homestead). Please check the instruction in it's page on how to deploy it.