<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailAuctionsWinners extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id, $mail_subject, $mail_body, $mail_signature)
    {
        $this->mail_subject = $mail_subject;
        $this->mail_body = $mail_body;
        $this->mail_signature = $mail_signature;
        $this->auction_id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.auctions.winner')
            ->subject($this->mail_subject)
            ->with('mail_body' ,$this->mail_body)
            ->with('auction_id', $this->auction_id)
            ->with('mail_signature' ,$this->mail_signature);
    }
}
