<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Auction extends Model
{
    /**
     * Public Scopes begin
     *
     * Default flag is true
     *
     **/
    // Return open auctions
    public function scopeOpen($query)
    {
        return $query->where('ended_at', null);
    }

    // Return open auctions
    public function scopeClosed($query)
    {
        return $query->where('ended_at', '<>', null);
    }

    /**
     * Public functions for Auctions
     *
     * Get bids for an auction
     */
    public function bids()
    {
        return $this->hasMany('App\Bid');
    }

    // Get the winner bid of an auction and return null if there is none
    public function winnerbid()
    {
        return $this->bids->where('winner', true)->first();
    }

    // Get the current highest bid for the Auction
    public function highestbid()
    {
        return $this->bids()->orderBy('amount', 'DESC')->first();
    }

    // Get the bidders
    public function bidders()
    {
        $bidders = collect([]);
        $bidonAuction = $this->bids->unique('user_id');
        foreach ($bidonAuction as $bid) {
            $bidders->push($bid->author());
        }
        return $bidders;
    }

    // Get category
    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id')->withTrashed()->first();
    }

    // Get Business Unit
    public function businessunit()
    {
        return $this->hasOne('App\Businessunit', 'id', 'businessunit_id')->withTrashed()->first();
    }

    // Get All Watches for this auction
    public function watches()
    {
        return $this->hasMany('App\Watch')->get();
    }

    // Get Image for this auction
    public function images()
    {
        return $this->hasMany('App\Image')->get();
    }

    // Get featured image for this auction
    public function featuredImage()
    {
        return $this->hasMany('App\Image')->where('featured', 1)->first();
    }

    // Returns wich user placed the bid
    public function createdBy()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->first();
    }

    // Get IR number or N/A when there is none defined
    public function getIR()
    {
        $result = isset($this->ir_number) ? $this->ir_number : 'N/A';
        return $result;
    }

    // Get remaining time for this auction
    public function timeRemain()
    {
        $results = "";
        if ($this->ended_at <> null) {
            $end_time = new Carbon($this->ended_at);
            $results = "Auction ended on " . $end_time->timezone('America/Chicago')->format('n/j/y \a\t g:i:s a T');
            return $results;
        } else {
            $end_time = Carbon::parse($this->expires_at)->timezone('America/Chicago');
            $now = Carbon::now()->timezone('America/Chicago');
            $interval = $end_time->diff($now);
            $days = $now->diffInDays($end_time);
            if ($days > 0) {
                $results = $interval->format('%dd %hh %im');
                return $results;
            } elseif ($days < 1) {
                $results = $interval->format('%hh %im %ss');
                return $results;
            } elseif ($this->ended_at <> null) {
                $results = "Auction ended";
                return $results;
            }
        }
    }

    public function selectWinnerOptions()
    {
        $results = array();
        $bids = $this->bids()->orderBy('amount', 'desc')->get()->unique('user_id');
        foreach ($bids as $bid) {
            $text = $bid->author()->name . ' - Bid amount: $' . $bid->amount;
            $results = array_add($results, $bid->id, $text);
        }
        $results = array_add($results, '0', 'No winner selected');
        return $results;
    }

    public function currentBidAmount()
    {
        $result = 0;
        if ($this->winnerBid() != null) {
            $result = $this->winnerBid()->amount;
        } elseif ($this->highestbid() != null) {
            $result = $this->highestbid()->amount;
        }
        return $result;
    }
}
