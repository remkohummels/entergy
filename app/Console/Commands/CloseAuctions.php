<?php

namespace App\Console\Commands;

use App\Auction;
use App\Events\AuctionComingToEnd;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;

class CloseAuctions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auctions:close {id?} {bid?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will check if there are expiring auctions and close them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now()->toDateTimeString();
        $interval = Carbon::now()->addMinute()->toDateTimeString();
        $within_an_hour = Carbon::now()->addMinutes(60)->toDateTimeString();

        // if not id argument is provided, it will run a batch check for expired in the currently open auctions
        if ($this->argument('id') == null) {
            Log::info('Running batch check of auctions which are about to close within an hour.');
            $alert_auctions = Auction::Open()
                                    ->where('notification_flag', false)
                                    ->whereBetween('expires_at', array($now, $within_an_hour))
                                    ->get();
            if ($alert_auctions->count() > 0) {
                Log::info('Will be about to close ' . $alert_auctions->count() . ' auctions within an hour.');
                foreach ($alert_auctions as $auc) {
                    Log::info('Notify all relevant users about Closing auction #' . $auc->id . ' - ' . $auc->item_name . ' within an hour.');
                    // Dispatch a new event to send the auction notification to be about to close within an hour.
                    event(new AuctionComingToEnd($auc));
                    $auc->notification_flag = true;
                    $auc->save();
                }
            } else {
                Log::info('No auctions will be notified to the relevant users about coming to end.');
            }

            Log::info('Running batch check of expired auctions to close');
            $auctions = Auction::Open()->whereBetween('expires_at', array($now, $interval))->get();
            if ($auctions->count() > 0) {
                Log::info('Will close ' . $auctions->count() . ' auctions');
                foreach ($auctions as $auction) {
                    Log::info('Closing auction #' . $auction->id . ' - ' . $auction->item_name);
                    $winnerbid = $auction->highestbid();
                    if ($winnerbid != null) {
                        $winnerbid->winner = true;
                        $winnerbid->save();
                        Log::info('Winner bid for auction #' . $auction->id . ' is bid #' . $winnerbid->id . ' for a total of $' . $winnerbid->amount);
                    } else {
                        Log::info('No winner bid was selected');
                    }
                    $auction->ended_at = $now;
                    $auction->save();
                }
            } else {
                Log::info('No auctions expired at ' . $now . ', batch check completed');
            }
        } else {
            // if an id argument is provided (user generated action), it will close the specific auction
            Log::info('(User generated) Closing auction #' . $this->argument('id'));
            $auction = Auction::Open()->find($this->argument('id'));
            if ($auction != null) {
                Log::info('Closing auction #' . $auction->id . ' - ' . $auction->item_name);
                if ($this->argument('bid') == null) {
                    $winnerbid = $auction->highestbid();
                } else {
                    $winnerbid = $auction->bids()->find($this->argument('bid'));
                }
                if ($winnerbid != null) {
                    $winnerbid->winner = true;
                    $winnerbid->save();
                    Log::info('Winner bid for auction #' . $auction->id . ' is bid #' . $winnerbid->id . ' for a total of $' . $winnerbid->amount);
                } else {
                    Log::info('No winner bid was selected');
                }
                $auction->ended_at = $now;
                $auction->save();
                return true;
            } else {
                Log::warning('Not open auction with id #' . $this->argument('id') . ' found. Exiting');
                return false;
            }
        }
    }
}
