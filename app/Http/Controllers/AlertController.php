<?php

namespace App\Http\Controllers;


use App\CategoryWatch;
use App\Watch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AlertController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category_watch_auctions = collect();
        $watch_auctions = collect();
        $subscribed_categories = collect();

        $bidded_auctions = Auth::user()->auctions();
        $category_watches = Auth::user()->category_watches();
        $watches = Auth::user()->watches();

        // get all auctions of categories which this user is watching
        foreach ($category_watches as $category_watch) {
            $items = $category_watch->category()->auctions();
            foreach ($items as $item) {
                if (is_null($item->ended_at)) {
                    $category_watch_auctions->push($item);
                }
            }
        }

        // get all auctions which this user is watching
        foreach ($watches as $watch) {
            $watch_auctions->push($watch->auction());
        }

        // filter only new auctions what this user did not bid on, but watching
        if (!is_null($category_watch_auctions)) {
            $category_new_auctions = $category_watch_auctions->diff($bidded_auctions)->sortByDesc('created_at');

            // collect the categories where new auctions are registered
            foreach ($category_new_auctions as $auc) {
                $subscribed_categories->push($auc->category());
            }
        }

        // remove the duplicates from subscribed_categories collection
        if (!is_null($subscribed_categories)) {
            $subscribed_categories = $subscribed_categories->unique();
        }

        // filter only out-bid, coming-to-end auctions
        $alert_auctions = $watch_auctions->filter(function ($value, $key) {
            $current_bid = Auth::user()->current_bid($value->id);
            if (is_null($current_bid)) {
                $current_bid_amount = $value->highestbid()->amount;
            } else {
                $current_bid_amount = $current_bid->amount;
            }
            if ($value->ended_at <> null) {
                if ($value->highestbid()->amount > $current_bid_amount) {
                    $value['alert_property'] = 'outbid';
                    return true;
                }
                return false;
            } else {
                $end_time = new Carbon($value->expires_at);
                $now = Carbon::now();
                $minutes = $now->diffInMinutes($end_time);
                if ($minutes < 60) {
                    if ($value->highestbid()->amount > $current_bid_amount) {
                        $value['alert_property'] = 'close_outbid';
                    } else {
                        $value['alert_property'] = 'close';
                    }
                    return true;
                }
                if ($value->highestbid()->amount > $current_bid_amount) {
                    $value['alert_property'] = 'outbid';
                    return true;
                }
                return false;
            }
        });

        if (!is_null($alert_auctions)) {
            $alert_auctions = $alert_auctions->sortByDesc('created_at');
        }

        return view('layouts.user_alerts')
            ->with('subscribed_categories', $subscribed_categories)
            ->with('category_new_auctions', $category_new_auctions)
            ->with('alert_auctions', $alert_auctions);
    }

    /**
     * Save the auction which is enabled for alerts.
     *
     * @return \Illuminate\Http\Response
     */
    public function auctionAlert(Request $request, $auctionId)
    {
        $notify_email = $request->input('notify_email');
        $notify_phone = $request->input('notify_phone');

        $watch = Watch::withTrashed()
            ->where('user_id', Auth::user()->id)
            ->where('auction_id', $auctionId)
            ->first();

        // check if this row exists
        if($watch) {
            $watch->restore();
        } else {
            $watch = new Watch;
            $watch->user_id = Auth::user()->id;
            $watch->auction_id = $auctionId;
        }

        // update the 'notify_email' field with checked-on or off
        if ($notify_email == 'on') {
            $watch->notify_email = 1;
        } else {
            $watch->notify_email = 0;
        }

        // update the 'notify_phone' field with checked-on or off
        if ($notify_phone == 'on') {
            $watch->notify_phone = 1;
        } else {
            $watch->notify_phone = 0;
        }

        $watch->save();

        // delete the row if 'notify_email' & 'notify_phone' was disabled
        if ($notify_email != 'on' && $notify_phone != 'on') {
            $watch->delete();
        }

        $request->session()->flash('message', 'Notification was enabled successfully.');
        $request->session()->flash('message-type', 'success');

        return redirect()->route('auction.detail', ['id' => $auctionId]);
    }

    /**
     * Save the category which is enabled for alerts.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryAlert(Request $request, $categoryId)
    {
        $notify_email = $request->input('notify_email');
        $notify_phone = $request->input('notify_phone');

        $category_watch = CategoryWatch::withTrashed()
            ->where('user_id', Auth::user()->id)
            ->where('category_id', $categoryId)
            ->first();

        // check if this row exists
        if($category_watch) {
            $category_watch->restore();
        } else {
            $category_watch = new CategoryWatch;
            $category_watch->user_id = Auth::user()->id;
            $category_watch->category_id = $categoryId;
        }

        // update the 'notify_email' field with checked-on or off
        if ($notify_email == 'on') {
            $category_watch->notify_email = 1;
        } else {
            $category_watch->notify_email = 0;
        }

        // update the 'notify_phone' field with checked-on or off
        if ($notify_phone == 'on') {
            $category_watch->notify_phone = 1;
        } else {
            $category_watch->notify_phone = 0;
        }

        $category_watch->save();

        // delete the row if 'notify_email' & 'notify_phone' was disabled
        if ($notify_email != 'on' && $notify_phone != 'on') {
            $category_watch->delete();
        }

        $request->session()->flash('message', 'Notification was enabled successfully.');
        $request->session()->flash('message-type', 'success');

        return redirect()->route('auction.category', ['id' => $categoryId]);
    }

}
