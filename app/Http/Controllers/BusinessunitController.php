<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Businessunit;

class BusinessunitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checkPrevious = Businessunit::onlyTrashed()->where('name', $request->businessunit_name)->first();
        if ($checkPrevious != null) {
            $checkPrevious->restore();
        } else {
            $businessunit = new Businessunit;
            if ($request->businessunit_name == null) {
                return redirect()->action('AdminSettingsController@index');
            }
            $businessunit->name = $request->businessunit_name;
            $businessunit->save();
        }
        return redirect()->action('AdminSettingsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $businessunit = Businessunit::find($id);
        if ($businessunit == null) {
            return redirect()->action('AdminSettingsController@index');
        }
        $businessunit->delete();
        return redirect()->action('AdminSettingsController@index');
    }
}
