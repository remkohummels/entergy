<?php

namespace App\Http\Controllers;

use App\Auction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailAuctionsWinners;

class AdminAuctionsEmailController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function editEmail($id, $type)
    {
        $auction = Auction::find($id);
        $user = $auction->winnerbid()->author();
        $mail_signature = 'Luanne Neeb Investment Recovery Services Specialist-Admin Entergy Algiers-Gretna Office, Supply Chain Dept.
                            1001 Virgil Street, Mail Unit L-VIR-701
                            Gretna, LA 70053
                            Phone (504)365-2947';

        switch ($type) {
            case 'winner':
                $mail_subject = 'Winner Notification';
                $mail_body = $user->name . ', this is to inform you that your Bid of $' .
                            $auction->highestBid()->amount . ' was the winning bid for Item #' .
                            $auction->id . ', ' . $auction->item_name .
                            '. Please send a check, made out to Entergy, to Luanne Neeb at the address below for the amount of your bid.  Once your check is received, ' .
                            $auction->contact_name .
                            ' will be authorized to release the item to you. You will be responsible for coordinating shipping and/or transportation of this item with ' .
                            $auction->contact_name . '.';
                break;
            case 'pickup':
                $mail_subject = 'Pick-Up Instructions';
                $mail_body = $user->name . ', Investment Recovery Services has received your check in the amount of $' .
                            $auction->highestBid()->amount .
                            ' for the above mentioned item.  Attached is a Bill of Sale for your records. (for vehicles only). Please contact ' .
                            $auction->contact_name . ' at ' . $auction->contact_phone .
                            ' to arrange a date and time to pick up the trailer.  We would appreciate it if you could pick it up within the next 10 days. ' .
                            $auction->contact_name . ', please release this trailer to ' . $user->name . '.';
                break;
            default:
                $mail_subject = '';
                $mail_body = '';
                break;
        }

        return view('layouts.admin_auctions_edit_email')
            ->with('auction', $auction)
            ->with('mail_subject', $mail_subject)
            ->with('mail_signature', $mail_signature)
            ->with('mail_body', $mail_body);
    }

    public function sendEmail(Request $request)
    {
        $this->validate($request, [
            'mail_to' => 'required|email|max:255',
            'mail_subject' => 'required|string|max:255',
            'mail_body' => 'required|string',
            'mail_signature' => 'required|string',
            'id' => 'required|integer'
        ]);
        $email = new EmailAuctionsWinners($id = $request->id, $mail_subject = $request->mail_subject, $mail_body = $request->mail_body, $mail_signature = $request->mail_signature);
        Mail::to($request->mail_to)->send($email);
        return redirect()->action(
            'AuctionController@show', ['id' => $request->id]
        );
    }
}
