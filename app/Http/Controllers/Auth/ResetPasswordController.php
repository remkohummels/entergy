<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\Mail\PasswordReset;
use Illuminate\Support\MessageBag;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     *  Over-ridden the reset method from the "SendsPasswordResetEmails" trait
     *  Remember to take care while upgrading laravel
     */
    public function reset(Request $request, MessageBag $errors)
    {
        $user = User::where('email', $request->input('email'))->first();

        // Check if the request email exists.
        if (is_null($user)) {
            $errors->add('email', 'This email address does not exist!');
            return redirect($request->path())
                ->withInput()
                ->withErrors($errors);
        }

        // Using database transactions is useful here because stuff happening is actually a transaction
        DB::beginTransaction();
        try
        {
            $new_password = str_random(8);
            $user->password = bcrypt($new_password);
            // Send a new generated password to user via email
            $email = new PasswordReset(new User(['password' => $new_password, 'name' => $user->name]));
            Mail::to($user->email)->send($email);
            $user->save();
            DB::commit();
            return redirect('login')
                ->with('email', $request->input('email'));
        }
        catch(Exception $e)
        {
            DB::rollback();
            return back();
        }
    }
}
