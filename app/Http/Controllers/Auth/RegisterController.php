<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerification;
use Illuminate\Support\MessageBag;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    protected $redirectTo = 'register/verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|full_name',
            'email' => 'required|email|email_domain|max:255|unique:users',
            'phone' => 'nullable|phone_number',
            'password' => 'required|min:8|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => "+1" . $data['phone'],
            'password' => bcrypt($data['password']),
            'verification_code' => mt_rand(1000, 9999),
        ]);
    }

    /**
     *  Over-ridden the register method from the "RegistersUsers" trait
     *  Remember to take care while upgrading laravel
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        // Using database transactions is useful here because stuff happening is actually a transaction
        DB::beginTransaction();
        try
        {
            event(new Registered($user = $this->create($request->all())));
            // After creating the user send an email with the 4-digits verification_code generated in the create method above
            $email = new EmailVerification(new User(['verification_code' => $user->verification_code, 'name' => $user->name]));
            Mail::to($user->email)->send($email);
            DB::commit();
            return redirect('register/verify');
        }
        catch(Exception $e)
        {
            DB::rollback();
            return back();
        }
    }

    /**
     * Display the verification code form.
     *
     * @param  array  $data
     * @return User
     */
    public function showVerify()
    {
        return view('auth.verification_code');
    }

    /**
     * Get the user who has the same token and change his/her status to verified i.e. 1
     */
    public function verify(Request $request, MessageBag $errors)
    {
        // check the verification code
        $user = User::where('verification_code', $request->input('verification_code'))->first();
        if (!is_null($user)) {
            $user->verified();
            $this->guard()->login($user);
        } else {
            $errors->add('verification_code', 'Invalid Code!');
            return redirect($request->path())
                ->withInput()
                ->withErrors($errors);
        }

        return redirect('login');
    }
}
