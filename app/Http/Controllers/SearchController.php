<?php

namespace App\Http\Controllers;

use App\Auction;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function auctions(Request $request)
    {
        if ($request->q != null) {
            if (\Request::is('admin/*')) {
                $admin_flag = true;
            } else {
                $admin_flag = null;
            }
            $auctions = Auction::where('item_name', 'like', '%' . $request->q . '%')->orWhere('item_description', 'like', '%' . $request->q . '%')->orWhere('ir_number', 'like', '%' . $request->q . '%')->get();
            return view('layouts.search_results')
                    ->with('search_term', $request->q)
                    ->with('admin_flag', $admin_flag)
                    ->with('search_count', $auctions->count())
                    ->with('auctions', $auctions);
        } else {
            if (\Request::is('admin/*')) {
                return redirect()->action('AdminDashboardController@index');
            } else {
                return redirect()->action('AuctionController@index');
            }
        }
    }

}
