<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    /**
     * Public Scopes begin
     *
     * Default flag is true
     *
     **/
    // Return winner bids
    public function scopeWinners($query, $flag = 1)
    {
        return $query->where('winner', $flag);
    }

    // Returns wich user placed the bid
    public function author()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->first();
    }

    // Return the auction where the bid was placed
    public function auction()
    {
        return $this->hasOne('App\Auction', 'id', 'auction_id')->first();
    }

    // Get the passed time of the user's latest bid
    public function passedTime()
    {
        $create_time = new Carbon($this->created_at);
        $suffix = "ago";
        $now = Carbon::now();
        $days = $now->diffInDays($create_time);
        $hours = $now->diffInHours($create_time);
        $minutes = $now->diffInMinutes($create_time);
        $results = "";
        if ($days > 0) {
            $results = $days . " days " . $suffix;
            return $results;
        } elseif ($hours > 0) {
            $results = $hours . " hours " . $suffix;
            return $results;
        } elseif ($minutes > 0) {
            $results = $minutes . " minutes " . $suffix;
            return $results;
        } else {
            $results = "a few seconds ago";
            return $results;
        }
    }
}
