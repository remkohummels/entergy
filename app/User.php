<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'name', 'email', 'phone', 'password', 'verification_code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
            'password', 'remember_token',
    ];

    /**
     * Public Scopes begin
     *
     * Default flag is true
     *
     * Return Admin User
     **/
    public function scopeAdmin($query, $flag = 1)
    {
        return $query->where('admin', $flag);
    }

    // Return Verified User
    public function scopeVerified($query, $flag = 1)
    {
        return $query->where('verified', $flag);
    }

    // Return Blocekd User
    public function scopeBlocked($query, $flag = 1)
    {
        return $query->where('blocked', $flag);
    }

    // Set the verified status to true and make the email token null
    public function verified()
    {
        $this->verified = 1;
        $this->verification_code = null;
        $this->save();
    }
    /*
     * Public Functions
    **/
    // Bids from this user
    public function bids()
    {
        return $this->hasMany('App\Bid');
    }

    // Winner bids from an User
    public function winnerbids()
    {
        return $this->bids()->where('winner', true)->get();
    }

    // Get the current bid of the user
    public function current_bid($auction_id)
    {
        return $this->bids()->where('auction_id', $auction_id)->orderBy('amount', 'desc')->first();
    }

    // Auctions where the user bidded
    public function auctions()
    {
        $auctions = collect([]);
        $bidonAuction = $this->bids->unique('auction_id');
        foreach ($bidonAuction as $bid) {
            $auctions->push($bid->auction());
        }
        return $auctions;
    }

    // Get All Watches from this user
    public function watches()
    {
        return $this->hasMany('App\Watch')->get();
    }

    // Get All Category Watches from this user
    public function category_watches()
    {
        return $this->hasMany('App\CategoryWatch')->get();
    }
}
