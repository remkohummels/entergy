<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryWatch extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * Public Scopes begin
     *
     * Default flag is true
     *
     **/
    // Return Active Watches
    public function scopeActive($query, $flag = 1)
    {
        return $query->where('active', $flag);
    }
    /*
     * Public Functions
    **/
    // Returns wich user owns this watch
    public function owner()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->first();
    }

    // Return the Category where the bid was placed
    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id')->first();
    }
}
