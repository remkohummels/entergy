<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Watch extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * Public Scopes begin
     *
     * Default flag is true
     *
     **/
    // Return Active Watches
    public function scopeActive($query, $flag = 1)
    {
        return $query->where('active', $flag);
    }
    /*
     * Public Functions
    **/
    // Returns wich user owns this watch
    public function owner()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->first();
    }

    // Return the auction where the watch was placed
    public function auction()
    {
        return $this->hasOne('App\Auction', 'id', 'auction_id')->first();
    }
}
